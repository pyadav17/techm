<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="main.css">
		<title>Login</title>
	</head>
	<body>
		<div class="container">
			<div class="login">
				<h1>Login</h1>
				@if(Session::has('message'))
					<p class="alert-info">{{ Session::get('message') }}</p>
				@endif

				<form method="post" action="/loginUser">
					@csrf
					<div class="login_inputs">
						<h5>Email</h5>
						<input type="email" name="email" value="{{old('email')}}" placeholder="email" id="email">
						@if($errors->has('email'))
						<span class="error">{{$errors->first('email')}}</span>
						@endif
					</div>
					<div class="login_inputs">
						<h5>Password</h5>
						<input type="password" name="password" value="{{old('email')}}" placeholder="password" id="password">
						@if($errors->has('password'))
						<span class="error">{{$errors->first('password')}}</span>
						@endif
					</div>
					<div class="login_inputs">
						<button>Login</button>
						<p>Click here if you dont have account? <a href="/signup">Register</a></p>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
