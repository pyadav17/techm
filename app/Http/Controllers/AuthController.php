<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;

class AuthController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function register(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ];
        $input = $request->all();
        $validate = Validator::make($input, $rules);
        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput();
        }
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        return redirect("/")->with('message', 'Sign-up Successfully! please login with email & passwod');
    }

    public function loginUser(Request $request)
    {
        $inputs = $request->all();
        $rules = ['email' => 'required', 'password' => 'required'];
        $validate = Validator::make($inputs, $rules);
        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput();
        }
        $userCredentials = $request->only('email', 'password');
        if (Auth::attempt($userCredentials)) {
            return redirect('/events');
        } else {
            return redirect('/')->with('message', 'User Credentials is not valide');
        }
    }
    public function signup()
    {
        return view('register');
    }

    public function dashboard()
    {
        return view('dashboard');
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}
