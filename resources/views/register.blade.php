<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="main.css">
		<title>Sign-up</title>
	</head>
	<body>
		<div class="container">
			<div class="login">
				<h1>Sign-up</h1>
				<form method="post" action="/register">
					@csrf
					<div class="login_inputs">
						<h5>Full Name</h5>
						<input type="name" name="name" value="{{old('name')}}" placeholder="Name" id="name">
						@if($errors->has('name'))
						<span class="error">{{$errors->first('name')}}</span>
						@endif
					</div>
					<div class="login_inputs">
						<h5>Email</h5>
						<input type="email" name="email" value="{{old('email')}}" placeholder="Email" id="email">
						@if($errors->has('email'))
						<span class="error">{{$errors->first('email')}}</span>
						@endif
					</div>
					<div class="login_inputs">
						<h5>Password</h5>
						<input type="password" name="password" placeholder="**************" id="password">
						@if($errors->has('password'))
						<span class="error">{{$errors->first('password')}}</span>
						@endif
					</div>
					<div class="login_inputs">
						<button>Login</button>
						<p>Click here if you have already account? <a href="/">Login</a></p>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
