@extends('master')
@section('container')
<div class="heading">
	<h1>Create Event</h1>
</div>
<div class="event_form">
	<form method="post" action="/eventsave">
		@csrf
		<div class="input_fields">
			<h5>Event Name</h5>
			<input type="text" value="{{old('event_name')}}" name="event_name" placeholder="Event Name">
			@if($errors->has('event_name'))
			<span class="error">{{$errors->first('event_name')}}</span>
			@endif
		</div>
		<div class="input_fields">
			<h5>Event Type</h5>
			<select name="event_type">
				<option value="">Select Event</option>
				<option value="Birthday">Birthday</option>
				<option value="Marriage">Marriage</option>
			</select>
			@if($errors->has('event_type'))
			<span class="error">{{$errors->first('event_type')}}</span>
			@endif
		</div>
		<div class="input_fields">
			<h5>Start Date</h5>
			<input type="date" value="{{old('start_date')}}" name="start_date" placeholder="Start date">
			@if($errors->has('start_date'))
			<span class="error">{{$errors->first('start_date')}}</span>
			@endif
		</div>
		<div class="input_fields">
			<h5>End Date</h5>
			<input type="date" value="{{old('end_date')}}" name="end_date" placeholder="End Date">
			@if($errors->has('end_date'))
			<span class="error">{{$errors->first('end_date')}}</span>
			@endif
		</div>
		<div class="input_fields">
			<button type="submit">Save</button>
		</div>
	</form>
@endsection
