@extends('master')
@section('container')
<div class="heading">
	<h1>Edit Event</h1>
</div>
<div class="event_form">
	<form method="post" action="/storeevent/{{$event->id}}">
		 @method('put')
        @csrf
		<div class="input_fields">
			<h5>Event Name</h5>
			<input type="text" value="{{$event->name}}" name="event_name" placeholder="Event Name">
			@if($errors->has('event_name'))
			<span class="error">{{$errors->first('event_name')}}</span>
			@endif
		</div>
		<div class="input_fields">
			<h5>Event Type</h5>
			<select name="event_type">
				<option value="">Select Event</option>
				<option @if($event->type == 'Birthday') selected @endif value="Birthday">Birthday</option>
				<option @if($event->type == 'Marriage') selected @endif value="Marriage">Marriage</option>
			</select>
			@if($errors->has('event_type'))
			<span class="error">{{$errors->first('event_type')}}</span>
			@endif
		</div>
		<div class="input_fields">
			<h5>Start Date</h5>
			<input type="date" value="{{$event->start_date}}" name="start_date" placeholder="Start date">
			@if($errors->has('start_date'))
			<span class="error">{{$errors->first('start_date')}}</span>
			@endif
		</div>
		<div class="input_fields">
			<h5>End Date</h5>
			<input type="date" value="{{$event->end_date}}" name="end_date" placeholder="End Date">
			@if($errors->has('end_date'))
			<span class="error">{{$errors->first('end_date')}}</span>
			@endif
		</div>
		<div class="input_fields_radio">
			<h5>Status</h5>
			<div class="status_class">
				<h5>Active</h5>
				<input type="radio" name="status" value="0" @if($event->status==0) checked @endif>
				<h5>De-Active</h5>
				<input type="radio" name="status" value="1" @if($event->status==1) checked @endif>
			</div>
		</div>
		<div class="input_fields">
			<button type="submit">Save</button>
		</div>
	</form>
@endsection
