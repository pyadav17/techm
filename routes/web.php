<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('register');
// });

Route::get('/', 'AuthController@login');
Route::get('/signup', 'AuthController@signup');
Route::post('/register', 'AuthController@register');
Route::post('/loginUser', 'AuthController@loginUser');
// Route::get('/dashboard', 'AuthController@dashboard');
Route::get('/logout', 'AuthController@logout')->middleware('islogin');

Route::get('/events', 'EventController@listItems')->middleware('islogin');
Route::get('/eventcreate', 'EventController@createItems')->middleware('islogin');
Route::post('/eventsave', 'EventController@saveItems')->middleware('islogin');
Route::get('/eventedit/{id}', 'EventController@editItems')->middleware('islogin');
Route::put('/storeevent/{id}', 'EventController@updateItems')->middleware('islogin');
Route::delete('/deleteevent/{id}', 'EventController@deleteItems')->middleware('islogin');
