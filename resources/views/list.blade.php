@extends('master')
@section('container')
<div class="heading">
	<h1>List Of Events</h1>
	<a href="/eventcreate"><i class="fas fa-plus-circle"></i>ADD EVENTS</a>
</div>
@if(Session::has('message'))
<p class="alert-info">{{ Session::get('message') }}</p>
@endif
<table class="table">
	<thead class="thead-dark">
		<tr>
			<th scope="col">#</th>
			<th scope="col">NAME</th>
			<th scope="col">TYPE</th>
			<th scope="col">START DATE</th>
			<th scope="col">END DATE</th>
			<th scope="col">STATUS</th>
			<th scope="col">ACTION</th>
		</tr>
	</thead>
	<tbody>
		@foreach($events as $index=>$event)
		<tr>
			<th scope="row">{{$index+1}}</th>
			<td>{{$event['name']}}</td>
			<td>{{$event['type']}}</td>
			<td>{{$event['start_date']}}</td>
			<td>{{$event['end_date']}}</td>
			<td>
				@if($event['status'] == 0)
				<a class="status">Active</a>
				@else
				<a class="statusDel">De-Active</a>
				@endif
			</td>
			<td>
				<a href="/eventedit/{{$event['id']}}"><i class="far fa-edit icustom"></i></a>
				<i onclick="confirmation({{$event['id']}})" class="far fa-trash-alt del icustom"></i>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection
@section('script')
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
	function confirmation(id){
	var result = confirm("Are you sure to delete?");
	if(result){
		$.ajax({
			url:'/deleteevent/'+id,
			type:"DELETE",
			 headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			 data:'',
			  success:function(res){
        		alert('Event Data Remove Successfully');
        		location.reload();
      }

		})
	}
}
</script>
@endsection
