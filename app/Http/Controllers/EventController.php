<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Redirect;

class EventController extends Controller
{
    public function listItems()
    {
        $events = Event::orderBy('id', 'desc')->get()->toArray();
        return view('list', compact('events'));
    }

    public function createItems(Request $request)
    {
        return view('add');
    }

    public function saveItems(Request $request)
    {
        $inputs = $request->all();
        $rules = ['event_name' => 'required', 'event_type' => 'required', 'start_date' => 'required', 'end_date' => 'required'];
        $validate = Validator::make($inputs, $rules);
        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput();
        }
        $data['name'] = $inputs['event_name'];
        $data['type'] = $inputs['event_type'];
        $data['start_date'] = $inputs['start_date'];
        $data['end_date'] = $inputs['end_date'];
        $event = Event::create($data);
        if ($event) {
            return redirect('/events')->with('message', 'Event Created Successfully');
        } else {
            return redirect('/eventcreate') > with('message', 'Event Not Created Please enter Valide date');
        }
    }

    public function editItems($id)
    {
        $event = Event::find($id);
        if (!$event) {
            return redirect('/events')->with('message', 'Event is Not exist. Please Select Valide Event');
        }
        return view('edit', compact('event'));
    }

    public function updateItems(Request $request, $id)
    {
        $rules = [
            'event_name' => 'required',
            'event_type' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
        $inputs = $request->all();
        $validate = Validator::make($inputs, $rules);
        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput();
        }
        $event = Event::find($id);
        if (empty($event)) {
            return redirect('/events')->with('message', 'This Event is Not exists.');
        }

        if (array_key_exists('status', $inputs)) {
            $event->status = $inputs['status'];
        }
        $event->name = $inputs['event_name'];
        $event->type = $inputs['event_type'];
        $event->start_date = $inputs['start_date'];
        $event->end_date = $inputs['end_date'];
        $check = $event->save();
        if ($check) {
            return redirect('/events')->with('message', 'Event ' . '"' . $inputs['event_name'] . '"' . ' is update Successfully');
        }
    }

    public function deleteItems($id)
    {
        $event = Event::find($id);
        $checks = $event->delete();
        return ['success' => "User Removed Successfully"];

    }
}
